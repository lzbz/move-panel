/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

const Main = imports.ui.main;
const LayoutManager = Main.layoutManager;
const MessageTray = Main.messageTray;
const Display = global.display;

class Extension {
	enable() {
		this._original_updateState = MessageTray._updateState;
		this._original_getDraggableWindowForPosition = Main.panel._getDraggableWindowForPosition;
		this._primary_monitor = LayoutManager.primaryMonitor;
		this.create_notifications_constraint(this._primary_monitor);
		this.patch_updateState();
		this.patch_getDraggableWindowForPosition();
		
		this._target_monitor = this.find_target_monitor()
		this.move_all(this._target_monitor);
	}

	disable() {
		MessageTray._updateState = this._original_updateState;
		Main.panel._getDraggableWindowForPosition = this._original_getDraggableWindowForPosition;
		delete MessageTray._constraint;
	}

	create_notifications_constraint(monitor) {
		const constraint = MessageTray.get_constraints()[0];
		if (constraint) {
			constraint.index = monitor.index;
			MessageTray._constraint = constraint;
		}
	}

	// To show notification on the second screen after moving the panel
	patch_updateState() {
		const patches = [
			{ from: 'Main.layoutManager.primaryMonitor.', to: 'Main.layoutManager.monitors[this._constraint.index].' },
			{ from: 'Main.', to: 'imports.ui.main.' },
			{ from: 'State.', to: 'imports.ui.messageTray.State.' },
			{ from: 'Urgency.', to: 'imports.ui.messageTray.Urgency.' },
		];
	
		const func = this._original_updateState.toString();
		MessageTray._updateState = this.patch_function(func, patches);
	}
	
	// To grab a window from the second screen after moving the panel (fixes #5)
	patch_getDraggableWindowForPosition() {
		const patches = [
			{ from: 'metaWindow.is_on_primary_monitor()', to: 'true' },
			{ from: 'Main.', to: 'imports.ui.main.' },
			{ from: 'Meta.', to: 'imports.gi.Meta.' },
		];
	
		const func = this._original_getDraggableWindowForPosition.toString();
		Main.panel._getDraggableWindowForPosition = this.patch_function(func, patches);
	}

	patch_function(func, patches) {
		let args = func.substring(func.indexOf('(') + 1, func.indexOf(')')).split(', ');
		let body = func.substring(func.indexOf('{') + 1, func.lastIndexOf('}'));
		for (const { from, to } of patches) {
			body = body.replaceAll(from, to);
		}

		return new Function(args, body);
	}

	find_target_monitor() {
		for (const monitor of LayoutManager.monitors) {
			if (monitor.index !== this._primary_monitor.index) {
				return monitor;
			}
		}
	}

	move_all(monitor) {
		if (this._primary_monitor.index !== monitor.index) {
			this.move_panel(monitor);
			this.move_hotcorners(monitor);
			this.move_notifications(monitor);
			this.fix_trayIconsReloaded();
		}
	}

	move_panel(monitor) {
		LayoutManager.panelBox.set_position(monitor.x, monitor.y);
		LayoutManager.panelBox.set_size(monitor.width, -1);
		LayoutManager.panelBox.visible = true;
	}
	
	move_hotcorners(monitor) {    
		const old_index = LayoutManager.primaryIndex;
		LayoutManager.primaryIndex = monitor.index;
		LayoutManager._updateHotCorners();
		LayoutManager.primaryIndex = old_index;
	}
	
	move_notifications(monitor) {    
		MessageTray._constraint.index = monitor.index;
	}

	// Rebuild tray icons to fix the problem with a icon placement when the top panel has been moved
	fix_trayIconsReloaded() {
		const extension = Main.extensionManager.lookup('trayIconsReloaded@selfmade.pl');
		if (extension && extension.state === ExtensionUtils.ExtensionState.ENABLED) {
			if (!extension.stateObj._rebuild) {
				extension.stateObj._rebuild = function() {
					this.TrayIcons._destroy();
					this.TrayIcons = new extension.imports.extension.TrayIconsClass(this._settings);
					this._setTrayMargin();
					this._setIconSize();
					this._setTrayArea();
				};
			}
	
			extension.stateObj._rebuild();
		}
	}
	
}

function init() {
	return new Extension();
}
